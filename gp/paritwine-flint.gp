install ("pari_fmpz_numbpart", "G", "fmpz_numbpart", LIBPARITWINESO);

install ("pari_arb_numbpart", "Gb", "arb_numbpart", LIBPARITWINESO);

install ("pari_acb_overlaps", "iGGb", "acb_overlaps", LIBPARITWINESO);
install ("pari_acb_contains", "iGGb", "acb_contains", LIBPARITWINESO);
install ("pari_acb_add", "GGb", "acb_add", LIBPARITWINESO);
install ("pari_acb_sub", "GGb", "acb_sub", LIBPARITWINESO);
install ("pari_acb_mul", "GGb", "acb_mul", LIBPARITWINESO);
install ("pari_acb_div", "GGb", "acb_div", LIBPARITWINESO);
install ("pari_acb_neg", "Gb", "acb_neg", LIBPARITWINESO);
install ("pari_acb_conj", "Gb", "acb_conj", LIBPARITWINESO);
install ("pari_acb_sqrt", "Gb", "acb_sqrt", LIBPARITWINESO);
install ("pari_acb_exp", "Gb", "acb_exp", LIBPARITWINESO);
install ("pari_acb_log", "Gb", "acb_log", LIBPARITWINESO);
install ("pari_acb_pow", "GGb", "acb_pow", LIBPARITWINESO);
install ("pari_acb_atan", "Gb", "acb_atan", LIBPARITWINESO);
install ("pari_acb_sin", "Gb", "acb_sin", LIBPARITWINESO);
install ("pari_acb_cos", "Gb", "acb_cos", LIBPARITWINESO);
install ("pari_acb_sinh", "Gb", "acb_sinh", LIBPARITWINESO);
install ("pari_acb_cosh", "Gb", "acb_cosh", LIBPARITWINESO);
install ("pari_acb_agm", "GGb", "acb_agm", LIBPARITWINESO);
install ("pari_acb_gamma", "Gb", "acb_gamma", LIBPARITWINESO);
install ("pari_acb_digamma", "Gb", "acb_digamma", LIBPARITWINESO);
install ("pari_acb_zeta", "Gb", "acb_zeta", LIBPARITWINESO);
install ("pari_acb_hurwitz_zeta", "GGb", "acb_hurwitz_zeta", LIBPARITWINESO);
install ("pari_acb_modular_eta", "Gb", "acb_modular_eta", LIBPARITWINESO);
install ("pari_acb_modular_j", "Gb", "acb_modular_j", LIBPARITWINESO);
install ("pari_acb_modular_delta", "Gb", "acb_modular_delta", LIBPARITWINESO);
install ("pari_acb_modular_eisenstein", "GLb", "acb_modular_eisenstein", LIBPARITWINESO);
install ("pari_acb_modular_theta", "GGb", "acb_modular_theta", LIBPARITWINESO);
install ("pari_acb_elliptic_invariants", "Gb", "acb_elliptic_invariants", LIBPARITWINESO);
install ("pari_acb_elliptic_p", "GGb", "acb_elliptic_p", LIBPARITWINESO);
install ("pari_acb_elliptic_p_prime", "GGb", "acb_elliptic_p_prime", LIBPARITWINESO);
install ("pari_acb_elliptic_inv_p", "GGb", "acb_elliptic_inv_p", LIBPARITWINESO);
install ("pari_acb_elliptic_zeta", "GGb", "acb_elliptic_zeta", LIBPARITWINESO);
install ("pari_acb_elliptic_sigma", "GGb", "acb_elliptic_sigma", LIBPARITWINESO);
install ("pari_acb_elliptic_k", "Gb", "acb_elliptic_k", LIBPARITWINESO);
install ("pari_acb_elliptic_e", "Gb", "acb_elliptic_e", LIBPARITWINESO);
install ("pari_acb_hypgeom_2f1", "GGGGLb", "acb_hypgeom_2f1", LIBPARITWINESO);

